#include <raylib.h>
#include <stdio.h>

const int WWIDTH = 512;
const int WHEIGHT = 432;
const char* WNAME = "little dumb project";

int main(int argc, char** argv){
    InitWindow(WWIDTH, WHEIGHT, WNAME);
    SetTargetFPS(60);
    int animFrames = 0;
    Image bg = LoadImageAnim("./src/res/frames.gif", &animFrames);
    Texture2D txBg = LoadTextureFromImage(bg);
    unsigned int nextFrameOfsset = 0;
    int currenteFrame = 0;
    int delayFrame = 9;
    int frameCuonter = 0;
    Model heart = LoadModel("./src/res/heart2.obj");
    Vector3 heartPos = {0.0f, -83.0f, -132.0f};
    Vector3 heartScale = {10.0f, 10.0f, 10.0f};
    float heartRotaionAngle = 1.0f;
    // float heartRotaionAngleSpeed = heartRotaionAngle; 
    BoundingBox heartBound = GetMeshBoundingBox(heart.meshes[0]);
    //rosca fodastica old code with some twists
    Camera cam = {0};
    cam.position = (Vector3){0.0f, 0.0f, 40.0f}; //here
    cam.up = (Vector3){0.0f, 1.0f, 0.0f};
    cam.target = (Vector3){0.0f, 1.0f, 0.0f};
    cam.projection = CAMERA_PERSPECTIVE;
    cam.fovy = 100.0f;
    //end of old code
    while(!WindowShouldClose()){
        frameCuonter++;
        if(frameCuonter >= delayFrame){
            currenteFrame++;
            if(currenteFrame >= animFrames) currenteFrame = 0;
            nextFrameOfsset = bg.width*bg.height*4*currenteFrame;
            UpdateTexture(txBg, ((unsigned char *)bg.data) + nextFrameOfsset);
            frameCuonter = 0;
        }
        heartRotaionAngle++;
        BeginDrawing();
            ClearBackground(WHITE);
            DrawTexture(txBg, 0, 0, WHITE);
            BeginMode3D(cam);
                DrawModelEx(heart, heartPos, (Vector3){0.0f, 1.0f, 0.0f}, heartRotaionAngle, heartScale, MAROON);
            EndMode3D();
        EndDrawing();
    }
    return 0;
}